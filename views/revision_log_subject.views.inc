<?php

/**
 * @file
 * A views inc file for allowing views to access the new DB column.
 */

/**
 * Implements hook_views_data_alter().
 */
function revision_log_subject_views_data_alter(&$data) {

  $data['node_revision']['log_subject'] = array(
    'title' => t("Subject of the revision's log"),
    'help' => t("Add a subject to your current revision's log message"),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

}
