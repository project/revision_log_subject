DESCRIPTION
===========
Provides a subject field to a node revision's log message.

SITE BUILDERS
=============
After activating revisions for your content type, 
you are going to have the option to give a subject 
to your revision log message.
This module integrates with Views, 
so that you can use the revision's log subject 
as a field in your Views.
